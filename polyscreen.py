#!/usr/bin/python

### Polyscreen
#
#
###

import os
import shutil
import sys
import subprocess
import pexpect
import yaml

class Screen:

    def __init__(self, config_file, profile=None):
        self.config_file = config_file
        self.profile = profile

    def get_config(self):

        with open(self.config_file, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exec:
                print(exec)

        return config

    def get_profile(self):
        profile = self.profile
        return profile

    def get_current_screens(self):
        # Get a list of the current screens
        screens = [l for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines()]
        return [l.split()[0] for l in screens if " connected " in l]

    def get_profile_screens(self):
        config = Screen.get_config(self)
        profile = Screen.get_profile(self)
        profile_screens = []
        for screenkey in config[profile]:
            for optionkey, value in config[profile][screenkey].items():
                if optionkey == 'name':
                    profile_screens.append(value)
        return profile_screens

    def compare_profiles(self):
        profile_screens = Screen.get_profile_screens(self)
        current_screens = Screen.get_current_screens(self)

        if len(profile_screens) < len(current_screens):
            remove_screens = list(set(current_screens) - set(profile_screens))
        else:
            remove_screens = []
        return remove_screens

    def list_profiles(self):
        config = Screen.get_config(self)
        profiles = []
        for screenkey in config:
            profiles.append(screenkey)
        return profiles

    def command(self):

        config = Screen.get_config(self)
        profile = Screen.get_profile(self)
        remove_screens = Screen.compare_profiles(self)

        full_command_list = []

        if len(remove_screens) > 0:
            for item in remove_screens:
                full_command_list.append('--output %s --off '%(item))

        for screenkey in config[profile]:
            for optionkey, value in config[profile][screenkey].items():
                if optionkey == 'name':
                    command = '--output %s '%(value)
                if optionkey == 'mode':
                    if value == 'auto':
                        command = command + '--auto '
                    else:
                        command = command + '--mode %s '%(value)
                if optionkey == 'primary':
                    if value == True:
                        command = command + '--primary '
                    else:
                        continue
                if optionkey == 'rotation':
                    command = command + '--rotate %s '%(value)
                if optionkey == 'position':
                    if value[1].isdigit():
                        command = command + '--pos %s '%(value)
                    else:
                        command = command + '--%s '%(value)
            full_command_list.append(command)

        return full_command_list

def find_xrandr():
    locate_xrandr = shutil.which('xrandr')

    if locate_xrandr == None:
        print("Please install xrandr to continue")
        exit(1)
    else:
        return locate_xrandr

def run_command(xrandr_command):
    failed_0 = '/usr/bin/xrandr: Configure crtc 1 failed'
    failed_1 = '/usr/bin/xrandr: Configure crtc 2 failed'
    failed_2 = '/usr/bin/xrandr: Configure crtc 3 failed'
    failed_3 = '/usr/bin/xrandr: Configure crtc 4 failed'

    failed_to_set = True
    first_run = True
    extra_crtc = 1

    while failed_to_set:
        failed_to_set = False

        if first_run:
            xrandr_run_command = pexpect.spawn(xrandr_command)
            first_run = False
        if not first_run:
            xrandr_run_command = pexpect.spawn(xrandr_command + '--crtc %i'%(extra_crtc))

        k = xrandr_run_command.expect([failed_0, failed_1, failed_2, failed_3, pexpect.EOF])
        if k == 0:
            print("crtc 1 failed")
            failed_to_set = True
            extra_crtc += 1
        if k == 1:
            print("crtc 2 failed")
            failed_to_set = True
            extra_crtc += 1
        if k == 2:
            print("crtc 3 failed")
            failed_to_set = True
            extra_crtc += 1
        if k == 3:
            print("crtc 4 failed")
        if k == 4:
            exit(1)

def main():
    user_home = os.getenv('HOME')
    config_file = '%s/.config/polyscreen/config.yaml'%(user_home)
    xrandr_path = find_xrandr()

    help_list = """ polyscreen.py - use xrandr to change screen configurations based on profiles
                Activate a profile: polyscreen.py <profile_name>
                List profiles from config: polyscreen.py profiles
                Print command: polyscreen.py command <profile_name>"""

    if len(sys.argv) == 1:
        print(help_list)
        exit(1)
    elif sys.argv[1] == 'profiles':
        polyscreen_profiles = Screen(config_file)
        print(polyscreen_profiles.list_profiles())
        exit(1)
    elif sys.argv[1] == 'command':
        profile = sys.argv[2]
    else:
        profile = sys.argv[1]

    polyscreen_things = Screen(config_file, profile)
    polyscreen_command = polyscreen_things.command()
    joined = ''.join(map(str, polyscreen_command))

    xrandr_command = '%s %s'%(xrandr_path, joined)

    if sys.argv[1] == 'command':
        print(xrandr_command)
    else:
        run_command(xrandr_command)

if __name__ == '__main__':
    main()
