# Polyscreen

## What is polyscreen?

Polyscreen is a Python program that I wrote in order to manage screen configurations. When using a WM such as i3/Xmonad/fluxbox etc. there is no good way to manage constant screen changes. One is left either saving long `xrandr` commands, or constantly using tools like `arandr` to manually set up screens. Polyscreen instead uses a simple YAML file where the user sets up "profiles". These profiles are then used in conjunction with `xrandr` to set up screens.

## Requirements

### Python Requirements
- Subprocess
- Pexpect
- YAML

### System Requirements
- xrandr

### Configuration Requirements
Once installed place the sample configuration file in `~/.config/polyscreen/config.yaml`. The following options are available:

| Option | Required | Values Accepted |
| ------ | -------- | --------------- |
| name   | yes      | name of output from `xrandr` |
| mode   | yes      | `auto`, `off`, or absolute resolution |
| primary | no      | `'yes'`, or `'no'` |
| position | no | relative position to the primary monitor or absolute position |
| rotation | no     | rotation of monitor (eg. `right`) |

## Installation
Copy `polyscreen.py` somewhere in your $PATH. Or link it for easy updates.

## Usage
Polyscreen only accepts a few commands:

`polyscreen profiles` lists the current profiles

`polyscreen <profile_name>` will attempt to set the current profile

`polyscreen command <profile_name>` will show the Xrandr command that will be run with said profile

## Warning!
There is currently no validation of your YAML config file! I *highly* recommend you run `polyscreen command <profile_name>` first to make sure everything looks correct. If you screw up your YAML, or your config in general, you could be left rebooting because you lost your screens.

## Disclaimer
THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## License
This project is licensed under the GNU GPLv3
